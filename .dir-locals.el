;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((nil
  (fill-column . 78)
  (tab-width . 8))
 (scheme-mode
  (indent-tabs-mode)
  (eval put 'make-parameter 'scheme-indent-function 1)
  (eval put 'with-db-worker-thread 'scheme-indent-function 1)
  (eval put 'with-time-logging 'scheme-indent-function 1)
  (eval put 'with-timeout 'scheme-indent-function 1)
  (eval put 'fibers-let 'scheme-indent-function 1)
  (eval . (put 'call-with-lzip-output-port 'scheme-indent-function 1))
  (eval . (put 'with-store 'scheme-indent-function 1))))
