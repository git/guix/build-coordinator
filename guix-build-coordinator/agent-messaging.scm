;;; Guix Build Coordinator
;;;
;;; Copyright © 2021 Christopher Baines <mail@cbaines.net>
;;;
;;; This file is part of the guix-build-coordinator.
;;;
;;; The Guix Build Coordinator is free software; you can redistribute
;;; it and/or modify it under the terms of the GNU General Public
;;; License as published by the Free Software Foundation; either
;;; version 3 of the License, or (at your option) any later version.
;;;
;;; The Guix Build Coordinator is distributed in the hope that it will
;;; be useful, but WITHOUT ANY WARRANTY; without even the implied
;;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;;; See the GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with the guix-data-service.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (guix-build-coordinator agent-messaging)
  #:use-module (oop goops)
  ;; #:use-module (guix-build-coordinator agent-messaging local)
  #:use-module (guix-build-coordinator agent-messaging http)
  #:duplicates (merge-generics))

(re-export submit-status)
(re-export submit-log-file)
(re-export submit-build-result)
(re-export report-build-start)
(re-export report-setup-failure)
(re-export submit-output)
(re-export fetch-builds-for-agent)

(re-export make-http-agent-interface)
