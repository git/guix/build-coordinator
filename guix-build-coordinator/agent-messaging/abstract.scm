;;; Guix Build Coordinator
;;;
;;; Copyright © 2021 Christopher Baines <mail@cbaines.net>
;;;
;;; This file is part of the guix-build-coordinator.
;;;
;;; The Guix Build Coordinator is free software; you can redistribute
;;; it and/or modify it under the terms of the GNU General Public
;;; License as published by the Free Software Foundation; either
;;; version 3 of the License, or (at your option) any later version.
;;;
;;; The Guix Build Coordinator is distributed in the hope that it will
;;; be useful, but WITHOUT ANY WARRANTY; without even the implied
;;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;;; See the GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with the guix-data-service.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (guix-build-coordinator agent-messaging abstract)
  #:use-module (ice-9 exceptions)
  #:export (make-agent-error-from-coordinator
            agent-error-from-coordinator?
            agent-error-from-coordinator-details))

(define-exception-type &agent-error-from-coordinator &error
  make-agent-error-from-coordinator
  agent-error-from-coordinator?
  (details agent-error-from-coordinator-details))
