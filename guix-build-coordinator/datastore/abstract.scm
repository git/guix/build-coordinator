(define-module (guix-build-coordinator datastore abstract)
  #:use-module (oop goops)
  #:export (<abstract-datastore>
            datastore-update
            datastore-store-derivation
            datastore-store-build
            datastore-list-agents
            datastore-find-agent
            datastore-new-agent
            datastore-new-agent-password
            datastore-list-agent-builds
            datastore-agent-password-exists?))

(define-class <abstract-datastore> ())

(define-generic datastore-store-derivation)
(define-generic datastore-store-build)
(define-generic datastore-new-agent)
(define-generic datastore-list-agents)
(define-generic datastore-find-agent)
(define-generic datastore-new-agent-password)
(define-generic datastore-update)
(define-generic datastore-agent-password-exists?)
(define-generic datastore-agent-list-unprocessed-builds)
(define-generic datastore-list-agent-builds)
