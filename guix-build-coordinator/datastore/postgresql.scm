(define-module (guix-build-coordinator datastore postgresql)
  #:use-module (oop goops)
  #:use-module (guix-build-coordinator datastore abstract)
  #:export (postgresql-datastore
            datastore-store-derivation))

(define-class <postgresql-datastore> (<abstract-datastore>))

(define (make-postgresql-datastore)
  (make <postgresql-datastore>))

(define-method (datastore-store-derivation
                (datastore <postgresql-datastore>)
                derivation)
  (peek "POSTGRESQL store derivation" datastore derivation))
