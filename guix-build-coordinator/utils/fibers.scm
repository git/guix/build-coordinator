(define-module (guix-build-coordinator utils fibers)
  #:use-module (srfi srfi-9)
  #:use-module (ice-9 q)
  #:use-module (ice-9 match)
  #:use-module (ice-9 atomic)
  #:use-module (ice-9 threads)
  #:use-module (ice-9 exceptions)
  #:use-module (ice-9 ports internal)
  #:use-module (ice-9 suspendable-ports)
  #:use-module (fibers)
  #:use-module (fibers timers)
  #:use-module (fibers channels)
  #:use-module (fibers scheduler)
  #:use-module (fibers operations)
  #:use-module (fibers conditions)
  #:use-module (knots timeout)
  #:use-module ((guix build syscalls)
                #:select (set-thread-name))
  #:use-module (guix-build-coordinator utils)
  #:export (spawn-port-monitoring-fiber

            make-discrete-priority-queueing-channels

            make-reusable-condition
            reusable-condition?
            signal-reusable-condition!
            reusable-condition-wait)
  #:replace (retry-on-error))

(define (spawn-port-monitoring-fiber port error-condition)
  (spawn-fiber
   (lambda ()
     (while #t
       (with-exception-handler
           (lambda (exn)
             (simple-format (current-error-port)
                            "port monitoring fiber failed to connect to ~A: ~A\n"
                            port exn)
             (signal-condition! error-condition)
             (sleep 10)
             (simple-format (current-error-port)
                            "port monitoring fiber error-condition unresponsive")
             (primitive-exit 1))
         (lambda ()
           (with-port-timeouts
            (lambda ()
              (let ((sock
                     (non-blocking-port
                      (socket PF_INET SOCK_STREAM 0))))
                (connect sock AF_INET INADDR_LOOPBACK port)
                (close-port sock)))
            #:timeout 20))
         #:unwind? #t)
       (sleep 20)))))

;; Use the fibers sleep
(define (retry-on-error . args)
  (apply
   (@ (guix-build-coordinator utils) retry-on-error)
   (append
    args
    (list #:sleep-impl sleep))))

(define (make-discrete-priority-queueing-channels channel num-priorities)
  (define all-queues
    (map (lambda _ (make-q))
         (iota num-priorities)))

  (define queue-channels
    (map (lambda _ (make-channel))
         (iota num-priorities)))

  (spawn-fiber
   (lambda ()
     (while #t
       (let loop ((queues all-queues))
         (let ((queue (car queues)))
           (if (q-empty? queue)
               (let ((next (cdr queues)))
                 (if (null? next)
                     (perform-operation
                      (apply
                       choice-operation
                       (map (lambda (queue queue-channel)
                              (wrap-operation (get-operation queue-channel)
                                              (lambda (val)
                                                (enq! queue val))))
                            all-queues
                            queue-channels)))
                     (loop next)))
               (let ((front (q-front queue)))
                 (perform-operation
                  (apply
                   choice-operation
                   (cons
                    (wrap-operation (put-operation channel front)
                                    (lambda _
                                      (q-pop! queue)))
                    (map (lambda (queue queue-channel)
                           (wrap-operation (get-operation queue-channel)
                                           (lambda (val)
                                             (enq! queue val))))
                         all-queues
                         queue-channels)))))))))))
  (apply values queue-channels))

(define-record-type <reusable-condition>
  (%make-reusable-condition atomic-box channel)
  reusable-condition?
  (atomic-box reusable-condition-atomic-box)
  (channel    reusable-condition-channel))

(define (make-reusable-condition)
  (%make-reusable-condition (make-atomic-box #f)
                            (make-channel)))

(define* (signal-reusable-condition! reusable-condition
                                     #:optional (scheduler (current-scheduler)))
  (match (atomic-box-compare-and-swap!
          (reusable-condition-atomic-box reusable-condition)
          #f
          #t)
    (#f
     (spawn-fiber
      (lambda ()
        (put-message (reusable-condition-channel reusable-condition)
                     #t))
      scheduler)
     #t)
    (#t #f)))

(define* (reusable-condition-wait reusable-condition
                                  #:key (timeout #f))
  (let ((val
         (if (atomic-box-ref (reusable-condition-atomic-box reusable-condition))
             #t
             ;; Not great as this is subject to race conditions, but it should
             ;; roughly work
             (if timeout
                 (perform-operation
                  (choice-operation
                   (get-operation (reusable-condition-channel reusable-condition))
                   (wrap-operation (sleep-operation timeout)
                                   (const #f))))
                 (get-message (reusable-condition-channel reusable-condition))))))
    (atomic-box-set! (reusable-condition-atomic-box reusable-condition)
                     #f)
    val))
