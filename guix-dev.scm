;;; guix-build-coordinator
;;;
;;; Copyright © 2020 Christopher Baines <mail@cbaines.net>
;;;
;;; This file is part of the guix-build-coordinator.
;;;
;;; guix-data-service is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; guix-data-service is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with the guix-data-service.  If not, see
;;; <http://www.gnu.org/licenses/>.

;;; Run the following command to enter a development environment for
;;; the guix-build-coordinator:
;;;
;;;  $ guix environment -l guix-dev.scm

(use-modules ((guix licenses) #:prefix license:)
             (guix packages)
             (guix download)
             (guix git-download)
             (guix utils)
             (guix build-system gnu)
             (gnu packages)
             (gnu packages autotools)
             (gnu packages compression)
             (gnu packages databases)
             (gnu packages gnupg)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (gnu packages package-management)
             (gnu packages perl)
             (gnu packages pkg-config)
             (gnu packages ruby)
             (gnu packages sqlite)
             (gnu packages texinfo)
             (srfi srfi-1))

(define guile-knots
  (let ((commit "d572f591a3c136bfc7b23160e16381c92588f8d9")
        (revision "1"))
    (package
    (name "guile-knots")
    (version (git-version "0" revision commit))
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://git.cbaines.net/git/guile/knots")
                    (commit commit)))
              (sha256
               (base32
                "0g85frfniblxb2cl81fg558ic3cxvla7fvml08scjgbbxn8151gv"))
              (file-name (string-append name "-" version "-checkout"))))
    (build-system gnu-build-system)
    (native-inputs
     (list pkg-config
           autoconf
           automake
           guile-3.0
           guile-lib
           guile-fibers))
    (inputs
     (list guile-3.0))
    (propagated-inputs
     (list guile-fibers))
    (home-page "https://git.cbaines.net/guile/knots")
    (synopsis "Patterns and functionality to use with Guile Fibers")
    (description
     "")
    (license license:gpl3+))))

(package
  (name "guix-build-coordinator")
  (version "0.0.0")
  (source #f)
  (build-system gnu-build-system)
  (inputs
   (list guix
         guile-json-4
         guile-fibers-1.3
         guile-knots
         guile-gcrypt
         guile-readline
         guile-lib
         guile-zlib
         guile-lzlib
         guile-prometheus
         (first (assoc-ref (package-native-inputs guix) "guile"))
         sqlite
         perl
         sqitch
         perl-dbd-sqlite))
  (native-inputs
   (list autoconf
         automake
         pkg-config))
  (synopsis "TODO")
  (description "TODO")
  (home-page "TODO")
  (license license:gpl3+))
