-- Deploy guix-build-coordinator:add_agent_names to sqlite

BEGIN;

ALTER TABLE agents ADD COLUMN name TEXT;

COMMIT;
