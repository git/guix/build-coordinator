-- Deploy guix-build-coordinator:add_agent_tags to sqlite

BEGIN;

CREATE TABLE agent_tags (
       agent_id TEXT NOT NULL REFERENCES agents (id),
       tag_id INTEGER NOT NULL REFERENCES tags (id),
       PRIMARY KEY (agent_id, tag_id)
);

COMMIT;
