-- Deploy guix-build-coordinator:add_agents_active to sqlite

BEGIN;

ALTER TABLE agents ADD COLUMN active NOT NULL DEFAULT 1;

COMMIT;
