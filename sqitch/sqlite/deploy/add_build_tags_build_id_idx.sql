-- Deploy guix-build-coordinator:add_build_tags_build_id_idx to sqlite

BEGIN;

CREATE INDEX build_tags_build_id_idx ON build_tags (build_id);

COMMIT;
