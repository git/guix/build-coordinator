-- Deploy guix-build-coordinator:add_builds_deferred_until to sqlite

BEGIN;

ALTER TABLE builds ADD COLUMN deferred_until TEXT;

COMMIT;
