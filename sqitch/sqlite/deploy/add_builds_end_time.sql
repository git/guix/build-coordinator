-- Deploy guix-build-coordinator:add_builds_end_time to sqlite

BEGIN;

ALTER TABLE builds ADD COLUMN end_time TEXT;

COMMIT;
