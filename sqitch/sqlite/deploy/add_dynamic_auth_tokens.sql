-- Deploy guix-build-coordinator:add_dynamic_auth_tokens to sqlite

BEGIN;

CREATE TABLE dynamic_auth_tokens (
       token TEXT NOT NULL
);

COMMIT;
