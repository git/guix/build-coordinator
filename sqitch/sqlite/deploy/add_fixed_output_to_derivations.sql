-- Deploy guix-build-coordinator:add_fixed_output_to_derivations to sqlite

BEGIN;

ALTER TABLE derivations
  ADD COLUMN fixed_output BOOLEAN
  CHECK (fixed_output IN (0,1));

COMMIT;
