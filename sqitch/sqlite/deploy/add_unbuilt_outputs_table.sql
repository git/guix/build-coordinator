-- Deploy guix-build-coordinator:add_unbuilt_outputs_table to sqlite

BEGIN;

CREATE TABLE unbuilt_outputs (output TEXT PRIMARY KEY);

INSERT INTO unbuilt_outputs
  SELECT output
  FROM derivation_outputs
  EXCEPT
    SELECT derivation_outputs.output
    FROM builds
    INNER JOIN build_results
      ON builds.uuid = build_results.build_id
    INNER JOIN derivation_outputs
      ON builds.derivation_name = derivation_outputs.derivation_name
    WHERE build_results.result = 'success';

COMMIT;
