-- Deploy guix-build-coordinator:allocator_related_indexes to sqlite

BEGIN;

CREATE INDEX builds_derivation_name_idx ON builds (derivation_name);

CREATE INDEX setup_failure_missing_inputs_setup_failure_id_idx
  ON setup_failure_missing_inputs (setup_failure_id);

COMMIT;
