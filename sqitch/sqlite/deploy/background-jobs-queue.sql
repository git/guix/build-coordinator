-- Deploy guix-build-coordinator:background-jobs-queue to sqlite

BEGIN;

CREATE TABLE background_jobs_queue (
       id INTEGER PRIMARY KEY,
       type TEXT NOT NULL,
       args TEXT NOT NULL
);

COMMIT;
