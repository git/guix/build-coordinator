-- Deploy guix-build-coordinator:build_allocation_agent_requested_systems to sqlite

BEGIN;

CREATE TABLE build_allocation_agent_requested_systems (
       agent_id NOT NULL REFERENCES agents (id),
       system TEXT NOT NULL,
       PRIMARY KEY (agent_id, system)
);

COMMIT;
