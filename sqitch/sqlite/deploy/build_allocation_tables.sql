-- Deploy guix-build-coordinator:build_allocation_tables to sqlite

BEGIN;

CREATE TABLE allocated_builds (
       build_id PRIMARY KEY NOT NULL REFERENCES builds (uuid),
       agent_id NOT NULL REFERENCES agents (id)
);

CREATE TABLE build_allocation_plan (
       build_id NOT NULL REFERENCES builds (uuid),
       agent_id NOT NULL REFERENCES agents (id),
       ordering INTEGER NOT NULL,
       PRIMARY KEY (agent_id, build_id)
);

COMMIT;
