-- Deploy guix-build-coordinator:build_counts to sqlite

BEGIN;

CREATE TABLE builds_counts AS
SELECT derivations.system_id, COUNT(*) AS count
FROM builds
INNER JOIN derivations
  ON builds.derivation_id = derivations.id
GROUP BY derivations.system_id;

COMMIT;
