-- Deploy guix-build-coordinator:build_results to sqlite

BEGIN;

CREATE TABLE build_results (
       build_id TEXT PRIMARY KEY NOT NULL REFERENCES builds (uuid),
       agent_id TEXT NOT NULL REFERENCES agents (id),
       result TEXT NOT NULL,
       failure_reason
);

COMMIT;
