-- Deploy guix-build-coordinator:build_results_counts to sqlite

BEGIN;

CREATE TABLE build_results_counts AS
SELECT agent_id, result, COUNT(*) AS count
FROM build_results
GROUP BY agent_id, result;

COMMIT;
