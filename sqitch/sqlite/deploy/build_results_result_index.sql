-- Deploy guix-build-coordinator:build_results_result_index to sqlite

BEGIN;

CREATE INDEX build_results_result_idx ON build_results (result);

COMMIT;
