-- Deploy guix-build-coordinator:build_starts to sqlite

BEGIN;

CREATE TABLE build_starts (
       id INTEGER PRIMARY KEY ASC,
       build_id TEXT NOT NULL REFERENCES builds (uuid),
       agent_id TEXT NOT NULL REFERENCES agents (id),
       start_time TEXT NOT NULL
);

COMMIT;
