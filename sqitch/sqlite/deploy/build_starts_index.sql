-- Deploy guix-build-coordinator:build_starts_index to sqlite

BEGIN;

CREATE INDEX build_starts_build_id_idx ON build_starts (build_id);

COMMIT;
