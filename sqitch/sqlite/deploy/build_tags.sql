-- Deploy guix-build-coordinator:build_tags to sqlite

BEGIN;

CREATE TABLE tags (
       id INTEGER PRIMARY KEY ASC,
       key TEXT NOT NULL,
       value TEXT NOT NULL
);

CREATE TABLE build_tags (
       build_id TEXT NOT NULL REFERENCES builds (uuid),
       tag_id INTEGER NOT NULL REFERENCES tags (id)
);

COMMIT;
