-- Deploy guix-build-coordinator:builds_created_at to sqlite

BEGIN;

ALTER TABLE builds ADD COLUMN created_at TEXT;

COMMIT;
