-- Deploy guix-build-coordinator:create_builds to sqlite

BEGIN;

CREATE TABLE builds (
       uuid TEXT PRIMARY KEY,
       derivation_name TEXT NOT NULL REFERENCES derivations (name),
       priority INTEGER NOT NULL,
       processed BOOLEAN NOT NULL DEFAULT 0
);

COMMIT;
