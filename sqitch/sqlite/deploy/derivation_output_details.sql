-- Deploy guix-build-coordinator:derivation_output_details to sqlite

BEGIN;

CREATE TABLE derivation_output_details (
       derivation_output_id INTEGER PRIMARY KEY ASC,
       hash_algorithm TEXT,
       hash TEXT,
       recursive BOOLEAN NOT NULL
);

COMMIT;
