-- Deploy guix-build-coordinator:derivation_outputs_derivation_name_index to sqlite

BEGIN;

CREATE INDEX derivation_outputs_derivation_name_idx ON derivation_outputs (derivation_name);

COMMIT;
