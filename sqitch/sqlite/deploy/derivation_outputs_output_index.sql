-- Deploy guix-build-coordinator:derivation_outputs_output_index to sqlite

BEGIN;

CREATE INDEX derivation_outputs_output_idx ON derivation_outputs (output);

COMMIT;
