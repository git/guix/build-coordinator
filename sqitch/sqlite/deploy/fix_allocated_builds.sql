-- Deploy guix-build-coordinator:fix_allocated_builds to sqlite

BEGIN;

DROP TABLE build_allocation_plan;

CREATE TABLE build_allocation_plan (
       build_id INTEGER NOT NULL REFERENCES builds (id),
       agent_id TEXT NOT NULL REFERENCES agents (id),
       ordering INTEGER NOT NULL,
       PRIMARY KEY (agent_id, build_id)
);

COMMIT;
