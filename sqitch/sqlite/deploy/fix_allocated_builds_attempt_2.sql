-- Deploy guix-build-coordinator:fix_allocated_builds_attempt_2 to sqlite

BEGIN;

DROP TABLE allocated_builds;

CREATE TABLE allocated_builds (
       build_id INTEGER PRIMARY KEY NOT NULL REFERENCES builds (id),
       agent_id TEXT NOT NULL REFERENCES agents (id)
);

COMMIT;
