-- Deploy guix-build-coordinator:fix_setup_failure_missing_inputs to sqlite

BEGIN;

DELETE FROM setup_failures WHERE failure_reason = 'setup_failure';

DROP TABLE setup_failure_missing_inputs;

CREATE TABLE setup_failure_missing_inputs (
       setup_failure_id INTEGER NOT NULL REFERENCES setup_failures (id),
       missing_input_store_path TEXT NOT NULL
);

COMMIT;
