-- Deploy guix-build-coordinator:initial_agent_tables to sqlite

BEGIN;

CREATE TABLE agents (
       id TEXT PRIMARY KEY,
       description TEXT
);

CREATE TABLE agent_passwords (
       id INTEGER PRIMARY KEY ASC,
       agent_id NOT NULL REFERENCES agents (id),
       password TEXT NOT NULL,
       timestamp DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE agent_status (
       id INTEGER PRIMARY KEY ASC,
       agent_id NOT NULL REFERENCES agents (id),
       timestamp DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
       status TEXT NOT NULL
);

COMMIT;
