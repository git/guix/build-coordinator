-- Deploy guix-build-coordinator:initial_derivation_tables to sqlite

BEGIN;

CREATE TABLE derivations (
       name TEXT PRIMARY KEY,
       system TEXT NOT NULL
);

CREATE TABLE derivation_outputs (
       id INTEGER PRIMARY KEY ASC,
       derivation_name TEXT NOT NULL REFERENCES derivations (name),
       name TEXT NOT NULL,
       output TEXT NOT NULL
);

CREATE TABLE derivation_inputs (
       derivation_name TEXT REFERENCES derivations (name),
       derivation_output_id INTEGER REFERENCES derivation_outputs (id),
       PRIMARY KEY (derivation_name, derivation_output_id)
);

COMMIT;
