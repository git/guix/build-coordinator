-- Deploy guix-build-coordinator:more_indexes_on_builds_and_derivation_inputs to sqlite

BEGIN;

CREATE INDEX derivation_inputs_derivation_output_id ON derivation_inputs (derivation_output_id);
CREATE INDEX builds_unprocessed ON builds (processed) WHERE processed = 0;

COMMIT;
