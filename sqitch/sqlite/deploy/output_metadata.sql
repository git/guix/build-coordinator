-- Deploy guix-build-coordinator:output_metadata to sqlite

BEGIN;

CREATE TABLE output_metadata (
       build_id TEXT NOT NULL REFERENCES builds (uuid),
       derivation_output_id INTEGER NOT NULL REFERENCES derivation_outputs (id),
       hash TEXT NOT NULL,
       size INTEGER NOT NULL,
       store_references TEXT NOT NULL
);

COMMIT;
