-- Deploy guix-build-coordinator:output_metadata_index to sqlite

BEGIN;

CREATE INDEX output_metadata_build_id_idx ON output_metadata (build_id);

COMMIT;
