-- Deploy guix-build-coordinator:remove_build_allocation_plan to sqlite

BEGIN;

DROP TABLE build_allocation_plan;

COMMIT;
