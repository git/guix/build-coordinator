-- Deploy guix-build-coordinator:replace_agent_status to sqlite

BEGIN;

DROP TABLE agent_status;

CREATE TABLE agent_status (
       agent_id TEXT PRIMARY KEY ASC REFERENCES agents (id),
       timestamp DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
       status TEXT NOT NULL,
       load_average_1min INTEGER
);

COMMIT;
