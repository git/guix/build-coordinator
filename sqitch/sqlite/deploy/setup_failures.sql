-- Deploy guix-build-coordinator:setup_failures to sqlite

BEGIN;

CREATE TABLE setup_failures (
       id INTEGER PRIMARY KEY ASC,
       build_id TEXT NOT NULL REFERENCES builds (uuid),
       agent_id TEXT NOT NULL REFERENCES agents (id),
       failure_reason TEXT NOT NULL
);

CREATE TABLE setup_failure_missing_inputs (
       setup_failure_id INTEGER NOT NULL REFERENCES setup_failures (id),
       missing_input_store_path TEXT NOT NULL
);

COMMIT;
