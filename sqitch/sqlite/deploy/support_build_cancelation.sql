-- Deploy guix-build-coordinator:support_build_cancelation to sqlite

BEGIN;

ALTER TABLE builds ADD COLUMN canceled BOOLEAN NOT NULL DEFAULT 0;

COMMIT;
