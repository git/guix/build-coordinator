-- Deploy guix-build-coordinator:unprocessed_hook_events to sqlite

BEGIN;

CREATE TABLE unprocessed_hook_events (
       id INTEGER PRIMARY KEY ASC,
       event TEXT NOT NULL,
       arguments TEXT NOT NULL
);

COMMIT;
